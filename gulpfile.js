const gulp = require('gulp'),
      compass = require('gulp-compass');
gulp.task('compass', function () {
    gulp.src('./src/*.scss')
        .pipe(compass({
            css: 'src/css',
            sass: 'src/scss'
        }))
        .pipe(gulp.dest('src/css'));
});


gulp.task('watch', function(){
    gulp.watch('app/scss/**/*.scss', ['compass']);
    // Other watchers
});