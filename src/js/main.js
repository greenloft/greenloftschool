/**
 * Created by K7 on 21.04.17.
 */
$(function () {
    $('.menuToggle').on('click', function () {
        //noinspection JSUnresolvedFunction
        $('.j-nav').stop(true, true).slideToggle(200);
    });
    $('.j-thumbnailsText,.j-subtitle,.j-subCattext').matchHeight();
    $('.j-subtitle').matchHeight();
    $('.j-subCattext').matchHeight();


    let header = $('.j-header');
    $(window).on("scroll", function () {
        if (header.offset().top > 250) {
            header.addClass('fixed');
        } else {
            header.removeClass('fixed');
        }
    });
    $(window).trigger("scroll");
});